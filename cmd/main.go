package main

import (
	"gitlab.com/botbin/modules/pkg/web"
	"go.uber.org/zap"
)

const (
	appName = "modules"
	// This is fetched by scripts. Maybe just don't move it anywhere.
	version = "0.4.5"
	port    = "8083"
)

func main() {
	zap.S().Infof("started %s v%s on port %s", appName, version, port)

	if err := web.NewServer().Start(port); err != nil {
		zap.S().Errorw("server error", "error", err)
	}
}

func init() {
	l, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	zap.ReplaceGlobals(l)
}
