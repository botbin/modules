PROJECT_NAME := modules

# Protocol buffer stuff
PROTO_INC := $(if $(GOROOT),$(GOROOT),~/go)/src

# Docker stuff
IMAGE_NAME := modules
COMPOSE_TEST_FILE := docker/docker-compose.test.yaml

# Variables for integration with the API gateway
GATEWAY_NAME := modules
GATEWAY_S3_BUCKET := $(if $(GATEWAY_S3_BUCKET),$(GATEWAY_S3_BUCKET),s3://config.gateway.dev.botbin.io)
GATEWAY_S3_REGION := $(if $(GATEWAY_S3_REGION),$(GATEWAY_S3_REGION),us-east-2)

build:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ${PROJECT_NAME} cmd/main.go

test:
	docker-compose -f ${COMPOSE_TEST_FILE} down -v || true
	docker-compose -f ${COMPOSE_TEST_FILE} up --build --abort-on-container-exit
	docker-compose -f ${COMPOSE_TEST_FILE} down -v

# Generates protobuf .go files in the package defined by their go_package option
proto:
	protoc -I ${PROTO_INC} --go_out ${PROTO_INC} ${PROTO_INC}/gitlab.com/botbin/${PROJECT_NAME}/schema/*.proto

# Uploads the API definition to the s3 bucket for the API gateway
gateway:
	aws s3 cp ./schema/gateway.toml ${GATEWAY_S3_BUCKET}/apis/${GATEWAY_NAME}/gateway.toml --region ${GATEWAY_S3_REGION}

version:
	$(eval VERSION=$(shell ./version.sh))

tag: version
	$(eval LATEST_TAG=v${VERSION})
	git tag -a v${LATEST_TAG} -m v${LATEST_TAG}

image: version
	docker build -f docker/Dockerfile -t ${REGISTRY}/${IMAGE_NAME}:${VERSION} .

deploy_image: image
	docker push ${REGISTRY}/${IMAGE_NAME}:${VERSION}

deploy: tag deploy_image
	git push origin ${LATEST_TAG}
	docker tag ${REGISTRY}/${IMAGE_NAME}:${VERSION} ${REGISTRY}/${IMAGE_NAME}:latest
	docker push ${REGISTRY}/${IMAGE_NAME}:latest

.PHONY: test proto image