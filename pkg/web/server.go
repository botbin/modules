package web

import (
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/modules/pkg/event"
	"gitlab.com/botbin/modules/pkg/module"
	"gitlab.com/botbin/modules/pkg/module/build"
	"gitlab.com/botbin/modules/pkg/module/search"
	"gitlab.com/botbin/modules/pkg/web/controller"
	"gitlab.com/botbin/modules/pkg/web/middleware"
	"go.uber.org/zap"
)

// Server is the root HTTP server for this service.
type Server struct {
	router      *gin.Engine
	middleware  []gin.HandlerFunc
	controllers []controller.Controller
}

// NewServer configures a Server with the default set of controllers.
func NewServer() *Server {
	brokers := strings.Split(os.Getenv("KAFKA_BROKERS"), ",")
	const groupID = "modules"
	requestPublisher := event.NewPublisher(brokers, "module.build.request")
	resultConsumer := event.NewConsumer(brokers, "module.build.result", groupID)
	statePublisher := event.NewPublisher(brokers, "module.state.change")

	moduleRepo := module.NewRepository()
	buildService := build.NewService(moduleRepo, requestPublisher, resultConsumer, statePublisher)
	buildService.StartDaemon()

	return &Server{
		middleware: []gin.HandlerFunc{
			middleware.NewClaimInjector().Get(),
		},
		controllers: []controller.Controller{
			controller.NewHealthCheck(),
			controller.NewSearch(search.NewExact(moduleRepo)),
			controller.NewBuildRequester(buildService),
		},
	}
}

// Start opens the HTTP server up to requests.
func (s *Server) Start(port string) error {
	s.createRouter()
	return http.ListenAndServe(":"+port, s.router)
}

func (s *Server) createRouter() {
	s.router = gin.Default()

	for _, mdl := range s.middleware {
		s.router.Use(mdl)
	}

	for _, ctrl := range s.controllers {
		ctrl.Accept(s.router)
		zap.S().Infof("using controller %T", ctrl)
	}
}
