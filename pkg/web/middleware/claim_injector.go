package middleware

import (
	"encoding/json"
	"errors"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/modules/pkg/web/controller"
	"go.uber.org/zap"
)

// ClaimInjector inserts JWT claims into the gin context.
type ClaimInjector struct {
	parser     *jwt.Parser
	parseChain []claimParser
}

type claimParser func(*gin.Context, jwt.MapClaims) error

var (
	errMissingBearerToken     = errors.New("missing token in bearer authorization header")
	errMissingSubClaim        = errors.New("missing 'sub' claim")
	errSubClaimWrongType      = errors.New("expected 'sub' claim to be an int")
	errMissingUserNameClaim   = errors.New("missing 'username' claim")
	errUserNameClaimWrongType = errors.New("expected 'username' claim to be a string")
	msgIdentityFailure        = gin.H{"message": "could not validate identity"}
)

// NewClaimInjector configures a ClaimInjector instance.
func NewClaimInjector() *ClaimInjector {
	injector := &ClaimInjector{
		parser: &jwt.Parser{UseJSONNumber: true},
	}
	injector.parseChain = []claimParser{
		injector.injectUserID,
		injector.injectUsername,
	}

	return injector
}

// Get returns a middleware that will inject the following values
// into the the gin context:
//
// Format: key - type - required - description
//
//			username - string - true - the "username" claim value
//		    userID   - int    - true - the "sub" claim value
//
// If any required value cannot be retrieved, the request will
// be aborted.
func (ci *ClaimInjector) Get() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		// TODO: Use a custom injectionless router for the health check controller.
		// That way this check won't be necessary.
		if ctx.Request.RequestURI == controller.HealthURI {
			return
		}

		claims, err := ci.getClaims(ctx)
		if err != nil {
			ci.abort(ctx, claims, err)
			return
		}

		for i := range ci.parseChain {
			err := ci.parseChain[i](ctx, claims)
			if err != nil {
				ci.abort(ctx, claims, err)
				return
			}
		}
	}
}

func (ci *ClaimInjector) abort(ctx *gin.Context, claims jwt.MapClaims, err error) {
	zap.S().Errorw("got malformed JWT from API gateway",
		"url", ctx.Request.URL.String(),
		"claims", claims,
		"error", err,
	)
	ctx.AbortWithStatusJSON(500, msgIdentityFailure)
}

func (ci *ClaimInjector) getClaims(ctx *gin.Context) (jwt.MapClaims, error) {
	authHeader := ctx.GetHeader("Authorization")
	parts := strings.Split(authHeader, " ")
	if len(parts) != 2 || parts[0] != "Bearer" {
		return nil, errMissingBearerToken
	}

	token, _, err := ci.parser.ParseUnverified(parts[1], jwt.MapClaims{})
	if err != nil {
		return nil, err
	}
	return token.Claims.(jwt.MapClaims), nil
}

func (ci *ClaimInjector) injectUserID(ctx *gin.Context, claims jwt.MapClaims) error {
	sub, exists := claims["sub"]
	if !exists {
		return errMissingSubClaim
	}

	num, ok := sub.(json.Number)
	if !ok {
		return errSubClaimWrongType
	}

	userID, err := num.Int64()
	if err != nil {
		return err
	}

	ctx.Set("userID", int(userID))
	return nil
}

func (ci *ClaimInjector) injectUsername(ctx *gin.Context, claims jwt.MapClaims) error {
	username, exists := claims["username"]
	if !exists {
		return errMissingUserNameClaim
	}

	forReal, ok := username.(string)
	if !ok {
		return errUserNameClaimWrongType
	}

	ctx.Set("username", forReal)
	return nil
}
