package middleware_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/modules/pkg/web/middleware"
)

// ClaimInjectorTestSuite runs tests against ClaimInjector.
type ClaimInjectorTestSuite struct {
	suite.Suite
}

func (cs *ClaimInjectorTestSuite) TestHandler() {
	testCases := []handlerTestCase{
		{
			ShouldFail: false,
			Claims:     jwt.MapClaims{"username": "test", "sub": 3},
		},
		{
			ShouldFail: true,
			Claims:     jwt.MapClaims{"username": 3, "sub": 3},
		},
		{
			ShouldFail: true,
			Claims:     jwt.MapClaims{"username": true, "sub": 3},
		},
		{
			ShouldFail: true,
			Claims:     jwt.MapClaims{"username": "test", "sub": "3"},
		},
		{
			ShouldFail: true,
			Claims:     jwt.MapClaims{"username": "test", "sub": true},
		},
		{
			ShouldFail: true,
			Claims:     jwt.MapClaims{"sub": 3},
		},
		{
			ShouldFail: true,
			Claims:     jwt.MapClaims{"username": "test"},
		},
		{
			ShouldFail: true,
			Claims:     jwt.MapClaims{},
		},
	}
	cs.runHandlerTests(testCases)
}

func (cs *ClaimInjectorTestSuite) runHandlerTests(tests []handlerTestCase) {
	for _, test := range tests {
		router := gin.New()
		router.Use(middleware.NewClaimInjector().Get())
		router.GET("/test", func(ctx *gin.Context) {})

		req, err := http.NewRequest("GET", "/test", nil)
		cs.Nil(err)
		req.Header.Set("Authorization", "Bearer "+cs.createJWT(test.Claims))

		rec := httptest.NewRecorder()
		router.ServeHTTP(rec, req)

		cs.True(test.ShouldFail != (rec.Code != 500))
	}
}

type handlerTestCase struct {
	Claims     jwt.MapClaims
	ShouldFail bool
}

func (cs *ClaimInjectorTestSuite) createJWT(claims jwt.MapClaims) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte("test"))
	cs.Nil(err)
	return tokenString
}

func TestClaimInjector(t *testing.T) {
	suite.Run(t, new(ClaimInjectorTestSuite))
}
