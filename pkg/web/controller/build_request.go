package controller

import (
	"encoding/base64"

	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/modules/pkg/module/build"
	"gitlab.com/botbin/modules/pkg/module/model"
	"go.uber.org/zap"
)

// BuildRequester handles HTTP requests to build new modules.
type BuildRequester struct {
	service build.Service
}

const (
	msgInvalidRequestBody = "invalid request body"
	msgFailedQueue        = "failed to queue module build"
	msgInvalidCode        = "code must be base64-encoded"
)

// NewBuildRequester creates a BuildRequester controller.
func NewBuildRequester(service build.Service) Controller {
	return &BuildRequester{service: service}
}

func (br *BuildRequester) Accept(r *gin.Engine) {
	r.POST("/v1/modules", br.request)
}

func (br *BuildRequester) request(ctx *gin.Context) {
	author := ctx.Value("username").(string)

	var err error
	view := new(model.View)
	if err = ctx.BindJSON(view); err != nil {
		br.reject(ctx, err, author, msgInvalidRequestBody)
		return
	}

	if err = br.decodeCode(view); err != nil {
		br.reject(ctx, err, author, msgInvalidCode)
		return
	}

	view.Author = author
	if err = br.service.Queue(view); err != nil {
		ctx.JSON(400, gin.H{"message": msgFailedQueue})
	}
}

func (br *BuildRequester) decodeCode(view *model.View) error {
	decoded, err := base64.StdEncoding.DecodeString(view.Code)
	if err == nil {
		view.Code = string(decoded)
	}
	return err
}

func (br *BuildRequester) reject(ctx *gin.Context, err error, user, reason string) {
	zap.S().Infow(reason, "user", user, "error", err)
	ctx.JSON(400, gin.H{"message": reason})
}
