package controller_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/modules/pkg/module/search/mocks"
	"gitlab.com/botbin/modules/pkg/web/controller"
)

type SearchTestSuite struct {
	suite.Suite
}

// Ensure search results that have no matches respond with [].
func (ss *SearchTestSuite) TestNonNil() {
	ss.runNonNilTests(
		url.Values{"q": []string{"test"}},
		url.Values{"author": []string{"test"}},
		url.Values{"author": []string{"test"}, "name": []string{"test"}},
		url.Values{"author": []string{"test"}, "name": []string{"test"}, "tag": []string{"test"}},
		url.Values{},
	)
}

func (ss *SearchTestSuite) runNonNilTests(queries ...url.Values) {
	for _, query := range queries {
		engine := new(mocks.Engine)
		engine.On("Find", mock.Anything).Return(nil)
		ctrl := controller.NewSearch(engine)
		router := gin.New()
		ctrl.Accept(router)

		req := ss.makeRequest(query)
		rec := httptest.NewRecorder()
		router.ServeHTTP(rec, req)

		body, err := ioutil.ReadAll(rec.Body)
		ss.Nil(err)
		ss.Equal("[]", string(body))

		engine.AssertExpectations(ss.T())
	}
}

func (ss *SearchTestSuite) makeRequest(query url.Values) *http.Request {
	r, err := http.NewRequest("GET", controller.SearchURI, nil)

	ss.Nil(err)
	vals := r.URL.Query()
	for k, v := range query {
		vals.Set(k, v[0])
	}
	r.URL.RawQuery = vals.Encode()

	return r
}

func TestSearch(t *testing.T) {
	suite.Run(t, new(SearchTestSuite))
}
