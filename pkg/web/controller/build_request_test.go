package controller_test

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/satori/go.uuid"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/modules/pkg/module/build/mocks"
	"gitlab.com/botbin/modules/pkg/module/model"
	"gitlab.com/botbin/modules/pkg/web/controller"
)

type BuildRequesterTestSuite struct {
	suite.Suite
}

func (bs *BuildRequesterTestSuite) TestRequest() {
	service := new(mocks.Service)
	service.
		On("Queue", mock.MatchedBy(func(v *model.View) bool {
			return v.Name == "fail"
		})).Return(errors.New("")).
		On("Queue", mock.MatchedBy(func(v *model.View) bool {
			return v.Name == "succeed"
		})).Return(nil)

	requester := controller.NewBuildRequester(service)
	router := gin.New()
	router.Use(func(ctx *gin.Context) { ctx.Set("username", "tester") })
	requester.Accept(router)

	bs.sendBuildRequest(router, []byte(""), 400)

	view := &model.View{Code: base64.StdEncoding.EncodeToString([]byte(""))}
	view.Name = "succeed"
	bs.sendBuildRequest(router, bs.bytes(view), 200)

	view.Name = "fail"
	bs.sendBuildRequest(router, bs.bytes(view), 400)

	view.Name = "succeed"
	view.Code = uuid.NewV4().String() // not base64
	bs.sendBuildRequest(router, bs.bytes(view), 400)

	service.AssertExpectations(bs.T())
}

func (bs *BuildRequesterTestSuite) bytes(v *model.View) []byte {
	b, err := json.Marshal(v)
	bs.Nil(err)
	return b
}

func (bs *BuildRequesterTestSuite) sendBuildRequest(r *gin.Engine, body []byte, expectedCode int) {
	req, err := http.NewRequest("POST", "/v1/modules", bytes.NewBuffer(body))
	bs.Nil(err)
	rec := httptest.NewRecorder()

	r.ServeHTTP(rec, req)
	bs.Equal(expectedCode, rec.Code)
}

func TestBuildRequester(t *testing.T) {
	suite.Run(t, new(BuildRequesterTestSuite))
}
