package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/modules/pkg/storage"
	"go.uber.org/zap"
)

// HealthURI is the URI for health checks.
const HealthURI = "/healthz"

// HealthCheck is a controller that handles service health checks.
type HealthCheck struct {
	Pingables []Pingable
}

// NewHealthCheck configures a controller for service health checks.
func NewHealthCheck() Controller {
	return &HealthCheck{
		Pingables: []Pingable{
			pgPingable{DB: storage.GetPGConn()},
		},
	}
}

// Accept registers the health check route with the gin engine.
func (hc *HealthCheck) Accept(r *gin.Engine) {
	r.GET(HealthURI, hc.handleHealthCheck)
}

func (hc *HealthCheck) handleHealthCheck(ctx *gin.Context) {
	for _, pingable := range hc.Pingables {
		if err := pingable.Ping(); err != nil {
			name := pingable.Name()

			zap.S().Errorw("ping check failed", "error", err, "service", name)
			ctx.AbortWithStatusJSON(500, map[string]string{
				"message": "service " + name + " is not responding",
			})
		}
	}
}
