package controller

import (
	"math"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/modules/pkg/module/model"
	"gitlab.com/botbin/modules/pkg/module/search"
)

// Search is a controller that handles module search queries.
type Search struct {
	engine      search.Engine
	blankResult []byte
	MaxLimit    int
	MaxLimitF   float64
}

// SearchURI defines where search requests are handled.
const SearchURI = "/v1/modules"

func NewSearch(e search.Engine) Controller {
	return &Search{
		engine:      e,
		blankResult: []byte("[]"),
		MaxLimit:    20,
		MaxLimitF:   20,
	}
}

func (es *Search) Accept(r *gin.Engine) {
	r.GET(SearchURI, es.get)
}

func (es *Search) get(ctx *gin.Context) {
	from := es.getFrom(ctx)
	limit := es.getLimit(ctx)

	results := es.executeQuery(from, limit, ctx)
	if results == nil {
		// Simulate a blank array to avoid allocation.
		ctx.Data(200, "application/json", es.blankResult)
	} else {
		ctx.JSON(200, results)
	}
}

// returns a from in [0, math.MaxInt]
func (es *Search) getFrom(ctx *gin.Context) int {
	from := ctx.Query("from")
	if from == "" {
		return 0
	}

	if i, err := strconv.Atoi(from); err != nil {
		return 0
	} else {
		return int(math.Max(0, float64(i)))
	}
}

// returns a limit in [0, es.MaxLimit]
func (es *Search) getLimit(ctx *gin.Context) int {
	limit := ctx.Query("limit")
	if limit == "" {
		return es.MaxLimit
	}

	if i, err := strconv.Atoi(limit); err != nil {
		return es.MaxLimit
	} else {
		return int(math.Max(0, math.Min(es.MaxLimitF, float64(i))))
	}
}

func (es *Search) executeQuery(from, limit int, ctx *gin.Context) []*model.View {
	if q := ctx.Query("q"); q != "" {
		return es.engine.Find(search.GenericQuery{
			Page:    search.Page{Limit: limit, Offset: from},
			Request: q,
		})
	}
	return es.engine.Find(search.DirectQuery{
		Page:   search.Page{Limit: limit, Offset: from},
		Author: ctx.Query("author"),
		Name:   ctx.Query("name"),
		Tag:    ctx.Query("tag"),
	})
}
