package controller_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/modules/pkg/web/controller"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

// HealthCheckControllerTestSuite manages tests for HealthCheckController.
type HealthCheckTestSuite struct {
	suite.Suite
}

// TestHealthZ ensures the health check endpoint responds with the
// desired status codes.
func (hs *HealthCheckTestSuite) TestHealthZ() {
	tests := []healthCheckCase{
		{
			ExpectedStatus: 200,
			Pingables: []controller.Pingable{
				mockPingable{false},
			},
		},
		{
			ExpectedStatus: 500,
			Pingables: []controller.Pingable{
				mockPingable{true},
			},
		},
		{
			ExpectedStatus: 500,
			Pingables: []controller.Pingable{
				mockPingable{false},
				mockPingable{true},
			},
		},
		{
			ExpectedStatus: 500,
			Pingables: []controller.Pingable{
				mockPingable{true},
				mockPingable{false},
			},
		},
	}

	hs.runHealthTests(tests)
}

func (hs *HealthCheckTestSuite) runHealthTests(tests []healthCheckCase) {
	for _, test := range tests {
		req, err := http.NewRequest("GET", controller.HealthURI, nil)
		assert.Nil(hs.T(), err)
		rec := httptest.NewRecorder()

		router := gin.New()
		ctrl := &controller.HealthCheck{Pingables: test.Pingables}
		ctrl.Accept(router)

		router.ServeHTTP(rec, req)
		assert.Equal(hs.T(), test.ExpectedStatus, rec.Code)
	}
}

type healthCheckCase struct {
	Pingables      []controller.Pingable
	ExpectedStatus int
}

type mockPingable struct {
	ShouldPingFail bool
}

func (mp mockPingable) Name() string {
	return "mock"
}

func (mp mockPingable) Ping() error {
	if mp.ShouldPingFail {
		return errors.New("mock error")
	}
	return nil
}

func TestHealthCheckController(t *testing.T) {
	suite.Run(t, new(HealthCheckTestSuite))
}
