package controller

import (
	"github.com/jmoiron/sqlx"
)

// A Pingable type uses pings to assess a service's health.
type Pingable interface {
	Name() string
	Ping() error
}

// pgPingable converts a sqlx database connection to a Pingable type.
type pgPingable struct {
	DB *sqlx.DB
}

func (pp pgPingable) Name() string {
	return "PostgreSQL"
}

func (pp pgPingable) Ping() error {
	return pp.DB.Ping()
}
