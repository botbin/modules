package module

import (
	"database/sql"
	"encoding/base64"

	"github.com/jmoiron/sqlx"
	"gitlab.com/botbin/modules/pkg/module/model"
	"gitlab.com/botbin/modules/pkg/module/serial"
	"gitlab.com/botbin/modules/pkg/storage"
	"go.uber.org/zap"
)

// A Repository manages a global collection of modules.
type Repository interface {
	Save(d *serial.Definition, l *serial.Location) (int64, error)
	FindViewByFQNParts(author, name, tag string) (*model.View, error)
	Find(query string, vals []interface{}) ([]*model.View, error)
}

type postgresRepo struct {
	db                *sqlx.DB
	createModule      *sql.Stmt
	createFunction    *sql.Stmt
	getViewByFQNParts *sqlx.Stmt
}

// NewRepository configures a Repository that manages modules.
func NewRepository() Repository {
	repo := new(postgresRepo)
	repo.db = storage.GetPGConn()
	repo.prepareStatements()
	return repo
}

// Save adds a module to the repository and returns its assigned ID.
func (pr *postgresRepo) Save(d *serial.Definition, l *serial.Location) (int64, error) {
	var id int64
	tx, err := pr.db.Begin()
	if err != nil {
		return id, err
	}

	meta := d.GetMeta()
	tx.Stmt(pr.createModule).QueryRow(meta.GetFqn(), meta.GetAuthor(), meta.GetName(),
		meta.GetTag(), meta.GetDescription()).Scan(&id)

	safeCode := base64.StdEncoding.EncodeToString([]byte(d.GetSpec().GetCode()))
	tx.Stmt(pr.createFunction).Exec(d.GetSpec().GetLanguage(), safeCode,
		l.GetFunctionUrl(), l.GetDockerImageUrl())

	err = tx.Commit()
	return id, err
}

// FindViewByFQNParts finds a module using the parts of its fully-qualified name.
// The code of results is base64 encoded.
func (pr *postgresRepo) FindViewByFQNParts(author, name, tag string) (*model.View, error) {
	view := new(model.View)
	err := pr.getViewByFQNParts.Get(view, author, name, tag)
	return view, err
}

// Find uses a SQL query string and arguments to search the repository.
// The code of results is base64 encoded.
func (pr *postgresRepo) Find(query string, vals []interface{}) ([]*model.View, error) {
	views := []*model.View{}
	err := pr.db.Select(&views, query, vals...)
	return views, err
}

// SelectViewBase is the base SQL query for selecting a module view.
const SelectViewBase = `
		SELECT m.id, m.created_at, m.fqn, m.author, m.name, m.tag, m.description,
			   l.language, f.code, f.api_url, f.docker_image_url
		FROM modules.modules m
		JOIN modules.functions f
		  ON f.module_id = m.id
		JOIN modules.languages l
		  ON l.id = f.language_id`

func (pr *postgresRepo) prepareStatements() {
	var err error
	pr.createModule, err = pr.db.Prepare(`
		INSERT INTO modules.modules (fqn, author, name, tag, description)
		VALUES ($1, $2, $3, $4, $5)
		RETURNING id
	`)
	checkStmtRes(err)

	pr.createFunction, err = pr.db.Prepare(`
		INSERT INTO modules.functions (module_id, language_id, code, api_url, docker_image_url)
		VALUES (
			(currval(pg_get_serial_sequence('modules.modules', 'id'))),
			(
				SELECT id FROM modules.languages
				WHERE language = $1
			),
			$2, $3, $4
		)
	`)
	checkStmtRes(err)

	pr.getViewByFQNParts, err = pr.db.Preparex(SelectViewBase + `
		WHERE m.author = $1
		  AND m.name   = $2
		  AND m.tag    = $3
	`)
	checkStmtRes(err)
}

func checkStmtRes(err error) {
	if err != nil {
		zap.S().Panicw("failed to prepare sql statement", "error", err)
	}
}
