package search

import (
	"fmt"
	"strings"

	"gitlab.com/botbin/modules/pkg/module"
)

// Page defines a request for paginated results.
type Page struct {
	Limit  int
	Offset int
}

// DirectQuery specifies key/value pairs to use in a predicate search.
// Blank fields will be ignored.
type DirectQuery struct {
	Page   Page
	Author string
	Name   string
	Tag    string
}

// SQL returns the query and values needed to execute a SQL statement.
func (dq DirectQuery) SQL() (string, []interface{}) {
	var vals []interface{}
	var clauses []string

	if len(dq.Author) > 0 {
		vals = append(vals, dq.Author)
		clauses = append(clauses, "m.author = $1")
	}
	if len(dq.Name) > 0 {
		vals = append(vals, dq.Name)
		clauses = append(clauses, fmt.Sprintf("m.name = $%d", len(vals)))
	}
	if len(dq.Tag) > 0 {
		vals = append(vals, dq.Tag)
		clauses = append(clauses, fmt.Sprintf("m.tag = $%d", len(vals)))
	}

	query := fmt.Sprintf(`%s WHERE %s ORDER BY m.created_at DESC LIMIT %d OFFSET %d`,
		module.SelectViewBase, strings.Join(clauses, " AND "), dq.Page.Limit, dq.Page.Offset)
	return query, vals
}

func (dq DirectQuery) isQuery() {}

// GenericQuery is a query whose search value has no context.
type GenericQuery struct {
	Page    Page
	Request string
}

func (gq GenericQuery) isQuery() {}
