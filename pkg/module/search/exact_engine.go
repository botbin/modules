package search

import (
	"errors"

	"gitlab.com/botbin/modules/pkg/module"
	"gitlab.com/botbin/modules/pkg/module/model"
	"go.uber.org/zap"
)

type exactEngine struct {
	repo module.Repository
}

var errUnsupportedQuery = errors.New("unsupported query")

// NewExact returns a search engine that peforms exact matches
// on the fields in a query. I.e., it does not utilize any
// full-text search.
func NewExact(repo module.Repository) Engine {
	return &exactEngine{repo: repo}
}

// Find gets all modules that match the given query.
func (ee *exactEngine) Find(query Query) (results []*model.View) {
	var err error
	switch q := query.(type) {
	case DirectQuery:
		if len(q.Author) > 0 && len(q.Name) > 0 && len(q.Tag) > 0 {
			var result *model.View
			result, err = ee.repo.FindViewByFQNParts(q.Author, q.Name, q.Tag)
			if err != nil {
				results = append(results, result)
			}
		} else {
			results, err = ee.repo.Find(q.SQL())
		}

	case GenericQuery:
		results, err = ee.repo.Find(DirectQuery{
			Page: q.Page,
			Name: q.Request,
		}.SQL())

	default:
		err = errUnsupportedQuery
	}

	if err != nil {
		ee.logFailure(query, err)
	}
	return results
}

func (ee *exactEngine) logFailure(query Query, err error) {
	zap.S().Infow("encountered search error",
		"engine", "exact",
		"query", query,
		"error", err,
	)
}
