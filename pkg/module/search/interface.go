package search

import "gitlab.com/botbin/modules/pkg/module/model"

// An Engine facilitates module search.
type Engine interface {
	Find(q Query) []*model.View
}

// A Query defines a search predicate.
type Query interface {
	isQuery()
}
