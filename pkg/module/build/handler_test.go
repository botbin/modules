package build_test

import (
	"errors"
	"testing"

	"github.com/satori/go.uuid"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/modules/pkg/module/build"
	mmocks "gitlab.com/botbin/modules/pkg/module/mocks"
	"gitlab.com/botbin/modules/pkg/module/serial"
	emocks "gitlab.com/botbin/modules/pkg/event/mocks"
)

type ResultSaverTestSuite struct {
	suite.Suite
}

func getGoodResult() *serial.Result {
	return &serial.Result{
		Definition: &serial.Definition{
			Meta: &serial.Metadata{
				Author: uuid.NewV4().String(),
				Name:   uuid.NewV4().String(),
				Tag:    uuid.NewV4().String(),
				Fqn:    uuid.NewV4().String(),
			},
		},
		Payload: &serial.Result_Success{
			Success: new(serial.Success),
		},
	}
}

func (rs *ResultSaverTestSuite) TestHandle_OK() {
	repo := new(mmocks.Repository)
	repo.On("Save", mock.Anything, mock.Anything).Return(int64(0), nil)
	pub := new(emocks.Publisher)
	pub.On("Publish", mock.Anything).Return(nil)

	saver := build.NewResultSaver(repo, pub)
	rs.Nil(saver.Handle(getGoodResult()))

	repo.AssertExpectations(rs.T())
	pub.AssertExpectations(rs.T())
}

// Ensure that unrecognized results are ignored.
func (rs *ResultSaverTestSuite) TestHandle_Unrecognized() {
	repo := new(mmocks.Repository)
	pub := new(emocks.Publisher)

	saver := build.NewResultSaver(repo, pub)
	rs.Nil(saver.Handle(&serial.Result{}))
	rs.Nil(saver.Handle(&serial.Result{
		Payload: &serial.Result_Failure{
			Failure: new(serial.Failure),
		},
	}))

	repo.AssertNotCalled(rs.T(), "Save")
	pub.AssertNotCalled(rs.T(), "Publish")
}

// Ensure that database errors are detected.
func (rs *ResultSaverTestSuite) TestHandle_Error_Repo() {
	repo := new(mmocks.Repository)
	repo.On("Save", mock.Anything, mock.Anything).Return(int64(0), errors.New(""))
	pub := new(emocks.Publisher)

	saver := build.NewResultSaver(repo, pub)
	rs.NotNil(saver.Handle(getGoodResult()))

	repo.AssertExpectations(rs.T())
}

// Ensure that event logging errors are detected.
func (rs *ResultSaverTestSuite) TestHandle_Error_Pub() {
	repo := new(mmocks.Repository)
	repo.On("Save", mock.Anything, mock.Anything).Return(int64(0), nil)
	pub := new(emocks.Publisher)
	pub.On("Publish", mock.Anything).Return(errors.New(""))

	saver := build.NewResultSaver(repo, pub)
	rs.NotNil(saver.Handle(getGoodResult()))

	repo.AssertExpectations(rs.T())
	pub.AssertExpectations(rs.T())
}

func TestResultSaver(t *testing.T) {
	suite.Run(t, new(ResultSaverTestSuite))
}
