package build

import (
	"database/sql"
	"errors"
	"fmt"
	"regexp"

	"gitlab.com/botbin/modules/pkg/module"
	"gitlab.com/botbin/modules/pkg/module/model"
)

type requestValidator struct {
	repo      module.Repository
	validName *regexp.Regexp
	validTag  *regexp.Regexp
}

var (
	errDuplicateModule = errors.New("a module already exists with this author, name, and tag")
	errBadName         = errors.New("module name is invalid")
	errBadTag          = errors.New("module tag is invalid")
)

func newRequestValidator(repo module.Repository) *requestValidator {
	return &requestValidator{
		repo:      repo,
		validName: regexp.MustCompile(`^[a-z0-9](-?[a-z0-9])*$`),
		validTag:  regexp.MustCompile(`^[a-z0-9]((-|\.)?[a-z0-9])*$`),
	}
}

func (rv *requestValidator) Check(view *model.View) error {
	if err := rv.verifyContent(view); err != nil {
		return err
	}
	return rv.verifyUniqueness(view)
}

func (rv *requestValidator) verifyContent(view *model.View) error {
	if len(view.Name) > 20 || !rv.validName.MatchString(view.Name) {
		return errBadName
	}

	if len(view.Tag) > 20 || !rv.validTag.MatchString(view.Tag) {
		return errBadTag
	}

	if view.Language != model.LanguageJS {
		return fmt.Errorf("%s is not a supported language", view.Language)
	}
	return nil
}

func (rv *requestValidator) verifyUniqueness(view *model.View) error {
	// TODO: Using a Redis bloom filter would be a really good idea here.
	_, err := rv.repo.FindViewByFQNParts(view.Author, view.Name, view.Tag)
	if err == nil {
		return errDuplicateModule
	} else if err != nil && err != sql.ErrNoRows {
		return err
	}
	return nil
}
