package build_test

import (
	"database/sql"
	"errors"
	"testing"

	"github.com/satori/go.uuid"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	emocks "gitlab.com/botbin/modules/pkg/event/mocks"
	"gitlab.com/botbin/modules/pkg/module/build"
	mmocks "gitlab.com/botbin/modules/pkg/module/mocks"
	"gitlab.com/botbin/modules/pkg/module/model"
)

type ServiceTestSuite struct {
	suite.Suite
}

// Ensure that modules with invalid fields are rejected.
func (ss *ServiceTestSuite) TestQueue_InvalidView() {
	ss.runInvalidChecks(
		viewCase{ // missing name
			Tag:      uuid.NewV4().String()[:20],
			Language: model.LanguageJS,
		},
		viewCase{ // name with spaces
			Name:     uuid.NewV4().String()[:18] + " t",
			Tag:      uuid.NewV4().String()[:20],
			Language: model.LanguageJS,
		},
		viewCase{ // missing tag
			Name:     uuid.NewV4().String()[:20],
			Language: model.LanguageJS,
		},
		viewCase{ // tag with spaces
			Name:     uuid.NewV4().String()[:20],
			Tag:      uuid.NewV4().String()[:18] + " t",
			Language: model.LanguageJS,
		},
		viewCase{ // missing language
			Name: uuid.NewV4().String()[:20],
			Tag:  uuid.NewV4().String()[:20],
		},
		viewCase{ // unrecognized language
			Name:     uuid.NewV4().String()[:20],
			Tag:      uuid.NewV4().String()[:20],
			Language: uuid.NewV4().String(),
		},
	)
}

func (ss *ServiceTestSuite) runInvalidChecks(tests ...viewCase) {
	for _, test := range tests {
		pub := new(emocks.Publisher)
		repo := new(mmocks.Repository)
		repo.On("FindViewByFQNParts", mock.AnythingOfType("string"),
			mock.AnythingOfType("string"), mock.AnythingOfType("string")).
			Return(new(model.View), sql.ErrNoRows)

		service := build.NewService(repo, pub, new(emocks.Consumer), new(emocks.Publisher))

		view := new(model.View)
		view.Author = uuid.NewV4().String()
		view.Name = test.Name
		view.Tag = test.Tag
		view.Language = test.Language
		
		err := service.Queue(view)
		ss.NotNil(err, "%+v", view)
	}
}

type viewCase struct {
	Name     string
	Tag      string
	Language string
}

// Ensure duplicate modules aren't queued.
func (ss *ServiceTestSuite) TestQueue_Duplicate() {
	pub := new(emocks.Publisher)
	repo := new(mmocks.Repository)
	repo.On("FindViewByFQNParts", mock.AnythingOfType("string"),
		mock.AnythingOfType("string"), mock.AnythingOfType("string")).
		Return(new(model.View), nil) // nil means a result was found

	service := build.NewService(repo, pub, new(emocks.Consumer), new(emocks.Publisher))
	err := service.Queue(ss.validView())
	ss.NotNil(err)

	repo.AssertExpectations(ss.T())
}

func (ss *ServiceTestSuite) validView() *model.View {
	v := new(model.View)
	v.Author = uuid.NewV4().String()
	v.Name = uuid.NewV4().String()[:20]
	v.Tag = uuid.NewV4().String()[:20]
	v.Language = model.LanguageJS
	return v
}

// Ensure publisher errors are detected.
func (ss *ServiceTestSuite) TestQueue_PublisherTimeout() {
	pub := new(emocks.Publisher)
	pub.On("Publish", mock.AnythingOfType("event.OutgoingMessage")).
		Return(errors.New(""))

	repo := new(mmocks.Repository)
	repo.On("FindViewByFQNParts", mock.AnythingOfType("string"),
		mock.AnythingOfType("string"), mock.AnythingOfType("string")).
		Return(new(model.View), sql.ErrNoRows)

	service := build.NewService(repo, pub, new(emocks.Consumer), new(emocks.Publisher))
	err := service.Queue(ss.validView())
	ss.NotNil(err)

	repo.AssertExpectations(ss.T())
	pub.AssertExpectations(ss.T())
}

func TestService(t *testing.T) {
	suite.Run(t, new(ServiceTestSuite))
}
