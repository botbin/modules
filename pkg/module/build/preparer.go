package build

import (
	"gitlab.com/botbin/modules/pkg/module/model"
	"gitlab.com/botbin/modules/pkg/module/serial"
)

type preparer struct {
}

func (p preparer) Prepare(v *model.View) *serial.Request {
	return &serial.Request{
		Definition: &serial.Definition{
			Meta: &serial.Metadata{
				Fqn:         v.Author + "/" + v.Name + ":" + v.Tag,
				Author:      v.Author,
				Name:        v.Name,
				Tag:         v.Tag,
				Description: v.Description,
			},
			Spec: &serial.Spec{
				Language: v.Language,
				Code:     v.Code,
			},
		},
	}
}
