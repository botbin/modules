package build

import (
	"github.com/golang/protobuf/proto"
	"gitlab.com/botbin/modules/pkg/module"
	"gitlab.com/botbin/modules/pkg/module/model"
	"gitlab.com/botbin/modules/pkg/module/serial"
	"gitlab.com/botbin/modules/pkg/event"
	"go.uber.org/zap"
)

// A Service provides access to module build capabilities.
type Service interface {
	Queue(view *model.View) error
	StartDaemon()
	// TODO: StopDaemon()
}

type service struct {
	queuePublisher event.Publisher
	resultConsumer event.Consumer
	saver          ResultHandler
	validator      *requestValidator
	prep           preparer
}

// NewService creates a Service that coordinates module builds between
// this and remote services.
func NewService(repo module.Repository, queuePublisher event.Publisher,
	resultConsumer event.Consumer, statePublisher event.Publisher) Service {
	return &service{
		validator:      newRequestValidator(repo),
		queuePublisher: queuePublisher,
		resultConsumer: resultConsumer,
		saver:          NewResultSaver(repo, statePublisher),
	}
}

// Queue adds view to a build queue if it is valid.
//
// Invalid views are those that have any of the following faults:
// - Duplicate author/name/tag tuple
// - Unsupported language
// - Missing name or tag fields
//
// The resulting error does not indicate if the build succeeded, only
// whether it could be queued.
func (s *service) Queue(view *model.View) error {
	if err := s.validator.Check(view); err != nil {
		return err
	}
	request := s.prep.Prepare(view)

	// TODO: This is slow when traffic is low.
	// Publishing is done in batches of 100 or once per second,
	// which ever comes first.
	// See https://godoc.org/github.com/segmentio/kafka-go#WriterConfig
	// for why that is, but basically it would be
	// a lot cooler if this was always pretty fast...
	return s.queuePublisher.Publish(event.OutgoingMessage{
		Key:   key(request.GetDefinition()),
		Value: request,
	})
}

// StartDaemon starts a goroutine that listens for module build events.
func (s *service) StartDaemon() {
	go func() {
		for message := range s.resultConsumer.Consume() {
			result := new(serial.Result)
			err := proto.Unmarshal(message.Value, result)
			if err != nil {
				zap.S().Errorw("got unexpected type when expecting result", "message", message)
				continue
			}

			go s.handleWithErr(result)
		}
	}()
}

func (s *service) handleWithErr(res *serial.Result) {
	if err := s.saver.Handle(res); err != nil {
		// TODO: Figure out a way to retry or notify the user.
		// The easier way to retry would be to resend the build
		// request. Notifications would involve publishing an
		// event and having the notification service transform it.
		zap.S().Errorw("failed to save module that was successfully built",
			"result", proto.MarshalTextString(res))
	}
}
