package build

import "gitlab.com/botbin/modules/pkg/module/serial"

// key retrieves a standard Kafka key for use with module-related events.
func key(d *serial.Definition) []byte {
	return []byte(d.GetMeta().GetFqn())
}
