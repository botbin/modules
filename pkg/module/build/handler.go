package build

import (
	"errors"

	"gitlab.com/botbin/modules/pkg/module"
	"gitlab.com/botbin/modules/pkg/module/serial"
	"gitlab.com/botbin/modules/pkg/event"
)

// ResultHandler processes build results.
type ResultHandler interface {
	Handle(*serial.Result) error
}

var errUnsupportedResult = errors.New("received incompatible result protobuf object")

type resultSaver struct {
	repo module.Repository
	pub  event.Publisher
}

// NewResultSaver creates a ResultHandler that saves successful results.
func NewResultSaver(repo module.Repository, pub event.Publisher) ResultHandler {
	return &resultSaver{
		repo: repo,
		pub:  pub,
	}
}

// Handle takes successful build results and saves them to a repository
// and event log. Unsuccessful or unrecognized results are simply ignored.
func (rs *resultSaver) Handle(res *serial.Result) error {
	switch res.GetPayload().(type) {
	case *serial.Result_Success:
		return rs.save(res)

	default:
		return nil
	}
}

func (rs *resultSaver) save(res *serial.Result) error {
	id, err := rs.repo.Save(res.GetDefinition(), res.GetSuccess().GetLocation())
	if err != nil {
		return err
	}

	module := &serial.Module{
		Id:         id,
		Definition: res.GetDefinition(),
		Location:   res.GetSuccess().GetLocation(),
	}

	return rs.pub.Publish(event.OutgoingMessage{
		Key: key(res.GetDefinition()),
		Value: &serial.StateChange{
			Event: &serial.StateChange_Creation{
				Creation: &serial.Creation{Module: module},
			},
		},
	})
}
