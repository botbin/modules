package build_test

import (
	"database/sql"
	"testing"
	"time"

	"github.com/satori/go.uuid"

	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/modules/pkg/module/build"
	mmocks "gitlab.com/botbin/modules/pkg/module/mocks"
	"gitlab.com/botbin/modules/pkg/module/model"
	"gitlab.com/botbin/modules/pkg/event"
	emocks "gitlab.com/botbin/modules/pkg/event/mocks"
)

// Tests the process of building a module using components
// from the build package.
type IntegrationTestSuite struct {
	suite.Suite
}

// Ensures a valid module can be built.
//
// This test uses a Service to queue a module in hopes that the module
// will make its way through the event system and be saved to the database.
// It tests the full event chain, including the build request, result, and
// module creation.
func (is *IntegrationTestSuite) TestPositive() {
	repo := new(mmocks.Repository)
	repo.
		On("FindViewByFQNParts", mock.Anything, mock.Anything, mock.Anything).Return(nil, sql.ErrNoRows).
		On("Save", mock.Anything, mock.Anything).Return(int64(1), nil)

	queuePublisher := new(emocks.Publisher)
	incomingMessages := make(chan event.IncomingMessage)
	queuePublisher.On("Publish", mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		outgoing := args[0].(event.OutgoingMessage)
		go func() { incomingMessages <- is.toIncoming(outgoing) }()
	})

	statePublisher := new(emocks.Publisher)
	done := make(chan bool)
	statePublisher.On("Publish", mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		go func() { done <- true }()
	})

	resultConsumer := new(emocks.Consumer)
	var left <-chan event.IncomingMessage = incomingMessages
	resultConsumer.On("Consume").Return(left)

	service := build.NewService(repo, queuePublisher, resultConsumer, statePublisher)
	service.StartDaemon()
	time.Sleep(time.Second)

	is.Nil(service.Queue(is.makeValidView()))

	select {
	case <-done:
	case <-time.NewTimer(time.Second).C:
		is.Fail("timed out waiting for build round trip")
	}

	repo.AssertExpectations(is.T())
	queuePublisher.AssertExpectations(is.T())
	statePublisher.AssertExpectations(is.T())
	resultConsumer.AssertExpectations(is.T())
}

func (is *IntegrationTestSuite) toIncoming(outgoing event.OutgoingMessage) event.IncomingMessage {
	result := getGoodResult()
	out, err := proto.Marshal(result)
	is.Nil(err)

	return event.IncomingMessage{
		Key:   outgoing.Key,
		Value: out,
	}
}

func (is *IntegrationTestSuite) makeValidView() *model.View {
	view := &model.View{Language: model.LanguageJS}
	view.Name = uuid.NewV4().String()[:20]
	view.Author = uuid.NewV4().String()
	view.Tag = uuid.NewV4().String()[:20]
	return view
}

func TestIntegration(t *testing.T) {
	suite.Run(t, new(IntegrationTestSuite))
}
