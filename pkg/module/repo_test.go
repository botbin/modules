package module_test

import (
	"testing"

	"github.com/satori/go.uuid"
	"gitlab.com/botbin/modules/pkg/module"
	"gitlab.com/botbin/modules/pkg/module/model"
	"gitlab.com/botbin/modules/pkg/module/search"
	"gitlab.com/botbin/modules/pkg/module/serial"
	"gitlab.com/botbin/modules/pkg/storage"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/suite"
)

type RepositoryTestSuite struct {
	suite.Suite
	db *sqlx.DB
}

func (rs *RepositoryTestSuite) SetupSuite() {
	rs.db = storage.GetPGConn()
}

func (rs *RepositoryTestSuite) SetupTest() {
	_, err := rs.db.Exec(`DELETE FROM modules.modules`)
	rs.Nil(err)
}

func (rs *RepositoryTestSuite) TestSave() {
	rs.saveRandom()
}

func (rs *RepositoryTestSuite) saveRandom() (string, string, string) {
	author := uuid.NewV4().String()
	name := uuid.NewV4().String()
	tag := uuid.NewV4().String()

	rs.save(author, name, tag)
	return author, name, tag
}

func (rs *RepositoryTestSuite) save(author, name, tag string) {
	d := &serial.Definition{
		Meta: &serial.Metadata{
			Author: author,
			Name:   name,
			Tag:    tag,
		},
		Spec: &serial.Spec{
			Language: model.LanguageJS,
			Code:     `function accept(p) { return p; }`,
		},
	}
	l := new(serial.Location)

	modules := module.NewRepository()
	id, err := modules.Save(d, l)
	rs.Nil(err)
	rs.NotEmpty(id)
}

func (rs *RepositoryTestSuite) TestFindViewByFQNParts() {
	author, name, tag := rs.saveRandom()

	modules := module.NewRepository()
	view, err := modules.FindViewByFQNParts(author, name, tag)
	rs.Nil(err)
	rs.Equal(author, view.Author)
	rs.Equal(name, view.Name)
	rs.Equal(tag, view.Tag)
}

func (rs *RepositoryTestSuite) TestFindViewsByAuthor() {
	author, _, _ := rs.saveRandom()

	modules := module.NewRepository()
	views, err := modules.Find(search.DirectQuery{
		Author: author,
		Page:   search.Page{Limit: 1, Offset: 0},
	}.SQL())
	rs.Nil(err)
	rs.Equal(author, views[0].Author)
}

func (rs *RepositoryTestSuite) TestFindViewsByAuthorName() {
	author, name, _ := rs.saveRandom()

	modules := module.NewRepository()
	views, err := modules.Find(search.DirectQuery{
		Author: author,
		Name:   name,
		Page:   search.Page{Limit: 1, Offset: 0},
	}.SQL())
	rs.Nil(err)
	rs.Equal(author, views[0].Author)
	rs.Equal(name, views[0].Name)
}

func (rs *RepositoryTestSuite) TestFindViewsByName() {
	_, name, _ := rs.saveRandom()

	modules := module.NewRepository()
	views, err := modules.Find(search.DirectQuery{
		Name: name,
		Page: search.Page{Limit: 1, Offset: 0},
	}.SQL())
	rs.Nil(err)
	rs.Equal(name, views[0].Name)
}

func TestModuleRespository(t *testing.T) {
	suite.Run(t, new(RepositoryTestSuite))
}
