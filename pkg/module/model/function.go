package model

// Function models the functions table in the postgres database.
type Function struct {
	ID             int64  `db:"id"`
	ModuleID       int64  `db:"module_id"`
	LanguageID     int    `db:"language_id"`
	Code           string `db:"code"`
	APIURL         string `db:"api_url"`
	DockerImageURL string `db:"docker_image_url"`
}
