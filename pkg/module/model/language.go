package model

// Language models the languages table in the postgres database.
type Language struct {
	ID       int    `db:"id"`
	Language string `db:"language"`
}

// LanguageJS is the standard string representation of the
// JavaScript language option.
const LanguageJS = "js"
