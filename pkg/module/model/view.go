package model

// View is a join on the modules, languages, and functions tables
// to produce a complete view of a module. When marshaled to JSON, it
// will only contain data deemed safe to end-user viewing. The Module
// proto definition should be used in contexts where all data is needed
// in a serializable format.
type View struct {
	Module
	Language       string `db:"language" json:"language"`
	Code           string `db:"code" json:"code"`
	APIURL         string `db:"api_url" json:"-"`
	DockerImageURL string `db:"docker_image_url" json:"-"`
}
