package model

import "time"

// Module models the modules table in the postgres database.
type Module struct {
	ID          int64     `db:"id" json:"id"`
	CreatedAt   time.Time `db:"created_at" json:"createdAt"`
	FQN         string    `db:"fqn" json:"fqn"`
	Author      string    `db:"author" json:"author"`
	Name        string    `db:"name" json:"name"`
	Tag         string    `db:"tag" json:"tag"`
	Description string    `db:"description" json:"description"`
}
