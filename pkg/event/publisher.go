package event

import (
	"context"

	"github.com/golang/protobuf/proto"
	"github.com/segmentio/kafka-go"
)

// A Publisher acts as the publisher in a pub/sub architecture.
type Publisher interface {
	Publish(m OutgoingMessage) error
	Close() error
}

type kafkaPublisher struct {
	writer *kafka.Writer
}

// NewPublisher creates a publisher that sends messages to a Kafka topic.
func NewPublisher(brokers []string, topic string) Publisher {
	writer := kafka.NewWriter(kafka.WriterConfig{
		Brokers:  brokers,
		Topic:    topic,
		Balancer: &kafka.LeastBytes{},
	})
	return &kafkaPublisher{writer: writer}
}

func (kp *kafkaPublisher) Publish(m OutgoingMessage) error {
	payload, err := proto.Marshal(m.Value)
	if err != nil {
		return err
	}

	return kp.writer.WriteMessages(context.Background(), kafka.Message{
		Key:   m.Key,
		Value: payload,
	})
}

func (kp *kafkaPublisher) Close() error {
	return kp.writer.Close()
}
