package event

import "github.com/golang/protobuf/proto"

// OutgoingMessage is the subject of publisher operations.
type OutgoingMessage struct {
	// Key forms a conceptual group around a class of messages.
	// For example, in RabbitMQ this could be a routing key; in
	// Kafka it would be the ordering key.
	Key []byte

	// Value contains the core content of the message.
	Value proto.Message
}

// IncomingMessage is the subject of consumer operations.
type IncomingMessage struct {
	Partition int
	Offset    int64
	Key       []byte
	Value     []byte
}
