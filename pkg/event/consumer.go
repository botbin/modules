package event

import (
	"context"
	"sync"

	kafka "github.com/segmentio/kafka-go"
	"go.uber.org/zap"
)

// Consumer acts as a subscriber in a pub/sub architecture.
type Consumer interface {
	Consume() <-chan IncomingMessage
	Close() error
}

type kafkaConsumer struct {
	reader         *kafka.Reader
	startConsuming sync.Once
	messages       chan IncomingMessage
	canceled       bool
}

// NewConsumer creates a consumer that reads messages from a Kafka topic.
func NewConsumer(brokers []string, topic, groupID string) Consumer {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers: brokers,
		GroupID: groupID,
		Topic:   topic,
	})

	return &kafkaConsumer{
		reader:   reader,
		messages: make(chan IncomingMessage),
	}
}

func (kc *kafkaConsumer) Consume() <-chan IncomingMessage {
	kc.startConsuming.Do(func() {
		go kc.loop()
	})
	return kc.messages
}

func (kc *kafkaConsumer) loop() {
	for {
		m, err := kc.reader.ReadMessage(context.Background())
		if err != nil {
			if kc.canceled {
				break
			}

			zap.S().Infow("failed to consume message", "error", err, "m", m)
			continue
		}

		kc.messages <- IncomingMessage{
			Partition: m.Partition,
			Offset:    m.Offset,
			Key:   m.Key,
			Value: m.Value,
		}
	}
}

func (kc *kafkaConsumer) Close() error {
	kc.canceled = true
	return kc.reader.Close()
}
