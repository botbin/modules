# modules
This service drives module creation and management through a user-facing
REST API and back-end system of event processing.

## Event Stream
Events are emitted to Kafka in response to build requests and when
the module context changes its state.

### Schemas
[Protobuf](https://github.com/google/protobuf) is used to serialize
outgoing messages. The schemas for this data are located [here](./schema).

### Topics
- module.build.request
    - Messages are sent here in response to requests to create modules.
- module.state.change
    - State changes, such as the creation or deletion of modules, are
      reflected through messages sent here.

## Tests
The tests are run using the `make test` command. This requires Docker
and Docker Compose to be installed.

## Environment Variables
- POSTGRES_URL - A connection string for PostgreSQL
- KAFKA_BROKERS - A comma-separated list of Kafka brokers