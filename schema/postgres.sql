/**
 * This schema was designed for PostgreSQL.
 *
 * The schema effectively belongs to this service, so changes
 * can be made as long as this service continues to fulfill
 * its API contracts.
 */

CREATE SCHEMA modules;

-- Holds the collection of programming languages in which module functions
-- can be written.
CREATE TABLE modules.languages (
    id serial PRIMARY KEY,
    language text UNIQUE NOT NULL
);
INSERT INTO modules.languages (language) VALUES ('js');

-- Contains the primary metadata about each module.
-- For the function context, see the `functions` table.
CREATE TABLE modules.modules (
    id bigserial PRIMARY KEY,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    -- The fully-qualified name
    fqn text UNIQUE NOT NULL,
    author text NOT NULL,
    name text NOT NULL,
    tag text NOT NULL,
    description text NOT NULL DEFAULT '',
    UNIQUE(author, name, tag)
);

-- Contains data regarding the executable function of each module.
CREATE TABLE modules.functions (
    id bigserial PRIMARY KEY,
    module_id bigint NOT NULL REFERENCES modules.modules(id) ON DELETE CASCADE,
    language_id int NOT NULL REFERENCES modules.languages(id) ON DELETE CASCADE,
    code text NOT NULL,
    api_url text UNIQUE NOT NULL,
    docker_image_url text UNIQUE NOT NULL
);
